<?php

namespace DL\HostsAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use DL\HostsAPIBundle\Entity\HostRange;

/**
 * HostRange controller.
 *
 * @Route("/")
 */
class HostRangeController extends Controller {

    /**
     *
     * @Route("hosts/update", name="updateHostsFile")
     */
    public function cronAction() {
        $em = $this->getDoctrine()->getManager();

        // get all hosts existing in the db
        $result = $em->getRepository('DLHostsAPIBundle:HostRange')->getAllHosts();

        // if no results are found, throw exception
        if (!$result) {
            throw $this->createNotFoundException('Unable to find HostRange entity.');
        }

        // hosts:all notation. This may need to be modified/made more sophisticated in future
        $hostGroup = array("hosts" => "ALL");

        // iterate through result set and add hosts:all notation
        foreach ($result as $host) {
            $jsonArray[$host['subnet'] . "/" . $host['cidr']] = json_encode($hostGroup, JSON_UNESCAPED_SLASHES);
        }

        // write json encoded hosts array to file
        file_put_contents('development-ips.json', json_encode($jsonArray, JSON_UNESCAPED_SLASHES));

        return new Response('Hosts Updated');
    }

}
