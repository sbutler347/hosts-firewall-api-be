<?php

namespace DL\HostsAPIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use DL\HostsAPIBundle\Entity\HostRange;
use DL\HostsAPIBundle\Form\HostRangeType;

class APIController extends FOSRestController {

    /**
     *
     * @Rest\Post("hosts/create")
     * @ApiDoc(
     *  resource=true,
     *  description="Post method to create a new range of hosts",
     *  statusCodes={
     *      400="Returned when errors",
     *      201="Returned when created"
     *  },    
     *  parameters={
     *      {"name"="name", "dataType"="string","required"=true,  "description"="name"},
     *      {"name"="subnet", "dataType"="string","required"=true,  "description"="subnet"},
     *      {"name"="cidr", "dataType"="string","required"=true,  "description"="cidr"},
     *      {"name"="description", "dataType"="string","required"=true,  "description"="description"},
     *      {"name"="enabled", "dataType"="boolean","required"=true,  "description"="enabled"},
     *  },
     * )
     * 
     */
    public function createHostRangeAction() {
        // create JSON value for hostsfile
        $hostGroup = array("hosts" => "ALL");

        $request = $this->getRequest();
        $data = json_decode($request->getContent());

        // create the host range object and the form
        $host = new HostRange();
        $form = $this->createForm(new HostRangeType(), $host, array("method" => "POST"));

        // bind the POSTed data to the form for validation
        $form->handleRequest($request);
        if ($form->isValid()) {

            // check if hosts file exists and, if not, create it with appropriate permissions
            if (file_exists('development-ips.json')) {

                // read the existing hosts file into an associated array
                $jsonArray = json_decode(file_get_contents('development-ips.json'), true);
            } else {

                $fp = fopen('development-ips.json', 'w+');
                fclose($fp);
            }

            // retrieve cidr value or assign as '32' if none provided
            if ($host->getCidr() == NULL) {
                $host->setCidr(32);
            }

            // if host is flagged as active, add to development-ips file
            if ($host->getEnabled() == 1) {

                // create the JSON host string to output and add to current array
                $jsonArray[$host->getSubnet() . "/" . $host->getCidr()] = json_encode($hostGroup, JSON_UNESCAPED_SLASHES);
            }

            // write the amdended hosts back to the exposed file
            file_put_contents('development-ips.json', json_encode($jsonArray, JSON_UNESCAPED_SLASHES));

            // persist the new host to the db
            $em = $this->getDoctrine()->getManager();
            $em->persist($host);
            $em->flush();

            // return an appropriate status code
            return new Response("Host created");
        } else {
            return new Response("Host note created");
        }
    }

    /**
     *
     * @Rest\Get("/hosts")
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Get method to return all hosts in .json format",
     *  statusCodes={
     *      400="Returned when errors",
     *      201="Returned when created"
     *  })
     */
    public function getHostsAction() {
        // pull all hosts in db and return these in JSON format
        $hosts = $this->getDoctrine()->getRepository('DLHostsAPIBundle:HostRange')
                ->findAll();
        $serializedEntity = $this->container->get('serializer')->serialize($hosts, 'json');
        return new Response($serializedEntity);
    }

    /**
     *
     * @Rest\Get("/hosts/active/{host}")
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Get method to check if supplied host is in a range and, if so, return the rangeId",
     *  statusCodes={
     *      400="Returned when errors",
     *      201="Returned when created"
     *  },
     * parameters={
     *      {"name"="host", "dataType"="string", "required"=true, "description"="host"}
     * }
     * )
     */
    public function getHostInRangeAction($host) {
        // getIsHostInRange() returns range ID if passed array is found within it
        $hosts = $this->getDoctrine()->getRepository('DLHostsAPIBundle:HostRange')
                ->getIsHostInRange(array('host' => $host));

        // if an ID is returned, set the response to 1. If an empty set is returned, set the response to 0
        if (!empty($hosts)) {
            $response = "Host currently active";
        } else {
            $response = "Host is not currently active";
        }

        // create the view and return this


        return new Response($response);
    }

    /**
     *
     * @Rest\Get("/hosts/{id}/enable")
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method to allow for enabling of a host. Returns string status",
     *  statusCodes={
     *      400="Returned when errors",
     *      201="Returned when created"
     *  },
     * parameters={
     *      {"name"="id", "dataType"="integer", "required"=true, "description"="id"}
     * }
     * )
     */
    public function enableHostAction($id) {
        $doc = $this->getDoctrine();

        // getIsHostInRange() returns range ID if passed array is found within it
        $host = $doc->getRepository('DLHostsAPIBundle:HostRange')
                ->findOneById($id);

        // check if host has been returned and, if not respond appropriately
        if (is_object($host)) {

            $host->setEnabled(true);
            $em = $doc->getManager();
            $em->persist($host);
            $em->flush();

            return $this->forward('DLHostsAPIBundle:HostRange:cron');
        } else {

            $response = "No Host found for that ID";

            // create the view and return this

            return new Response("Host Enabled");
        }
    }

    /**
     *
     * @Rest\Get("/hosts/{id}/disable")
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Method to allow for disabling of a host. Returns string status",
     *  statusCodes={
     *      400="Returned when errors",
     *      201="Returned when created"
     *  },
     * parameters={
     *      {"name"="id", "dataType"="integer", "required"=true, "description"="id"}
     * }
     * )
     */
    public function disableHostAction($id) {
        $doc = $this->getDoctrine();

        // getIsHostInRange() returns range ID if passed array is found within it
        $host = $doc->getRepository('DLHostsAPIBundle:HostRange')
                ->findOneById($id);

        // check if host has been returned and, if not respond appropriately
        if (is_object($host)) {

            $host->setEnabled(false);
            $em = $doc->getManager();
            $em->persist($host);
            $em->flush();

            return $this->forward('DLHostsAPIBundle:HostRange:cron');
        } else {

            $response = "No Host found for that ID";

            return new Response($response);
        }
    }

}
