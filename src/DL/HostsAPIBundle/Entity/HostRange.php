<?php

namespace DL\HostsAPIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * HostRange
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="DL\HostsAPIBundle\Entity\HostRangeRepository")
 */
class HostRange
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @Assert\Ip
     * @ORM\Column(name="subnet", type="string")
     */
    private $subnet;

    /**
     * @var integer
     * @Assert\Range(min = 1, max = 32, minMessage = "Cidr value must be equal to or more than 1",maxMessage = "Cidr value must be equal to or less than 32");
     * @ORM\Column(name="cidr", type="string")
     */
    private $cidr;

    /**
     * @var \DateTime $created_at
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $created_at;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return HostRange
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set subnet
     *
     * @param integer $subnet
     * @return HostRange
     */
    public function setSubnet($subnet)
    {
        $this->subnet = $subnet;

        return $this;
    }

    /**
     * Get subnet
     *
     * @return integer 
     */
    public function getSubnet()
    {
        return $this->subnet;
    }

    /**
     * Set cidr
     *
     * @param integer $cidr
     * @return HostRange
     */
    public function setCidr($cidr)
    {
        $this->cidr = $cidr;

        return $this;
    }

    /**
     * Get cidr
     *
     * @return integer
     */
    public function getCidr()
    {
        return $this->cidr;
    }


    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return HostRange
     */
    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreated_at()
    {
        return $this->created_at;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return HostRange
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return boolean
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }


}
