## Firewall API
 
### Routes:

*/hosts
*
	Returns a JSON view of all the hosts currently held in the database

*/hosts/active/{hostIp}
*
	GET Route which returns 1 or 0 depending on whether the specified host is present in the db

*/hosts/create
*
	POST Route which takes a hostRange object and writes it to the development-ips.json file and the database.  This takes the following params:

		- name
		- subnet : must be an IP address
		- cidr : for range notation.  Must be between 1-32 if provided. If not, defaults to 32
		- description
		- active : value must be boolean

*/hosts/update
*
	Updates the development-ips.json file with all hosts currently in the database. 
	Mainly to be used for a daily cron job sync

*/hosts/enable/{id}
*
	Enable the given host

*/hosts/disable/{id}
*
	Disable the given host
	
       